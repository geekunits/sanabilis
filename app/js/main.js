$(function() {

	/*
	* main slider
	*/

	$('.owl-main-slider').owlCarousel({
		nav:true,
		dots:false,
		margin:0,
		items: 1,
		loop: true
	});

	/*
	* gallery
	*/

	$("a.gallery-funcy").fancybox();

	/*
	* reviews
	*/

	$(document).ready(function() {
		$(".feedbacks__item-video-wrap, .video__wrap").fancybox({
			maxWidth    : 800,
			maxHeight    : 600,
			fitToView    : false,
			width        : '70%',
			height        : '70%',
			autoSize    : false,
			closeClick    : false,
			openEffect    : 'none',
			closeEffect    : 'none'
		});
	});

	/*
	* menu
	*/

	$('.header-xs__btn').on('click', function (e) {
		e.preventDefault();
		var $this = $(this);

		$this.parent().toggleClass('open');
		$('body').toggleClass('fixed');
		$('.header').toggleClass('header_border-bottom');
		$this.siblings('.header-xs__menu-dropdown').first().stop(true, true).fadeToggle(300);
	});

	/*
	* pages-nav
	*/

	$(window).on('load', function (){
		$('.owl-page-nav').owlCarousel({
			nav:false,
			dots:false,
			margin:20,
			loop: false,
			autoWidth: true
		});
	});

	/*
	* certigications
	*/

	$("a.certifications__link").fancybox();


	/* 
	* change div file name attach file 
	*/

	$('#reviews__file').on("change", function(e) {
		imgNameToDiv(); 
	});

	var imgNameToDiv = function(e) { 
		var indexForSlice = $('#reviews__file').val().lastIndexOf('\\'); 	
		$('.reviews__label-file').html($('#reviews__file').val().substring(indexForSlice+1)); 
	};

	/*
	* magazine
	*/

	$('.magazine__item').hover(function() {
		var $this = $(this);

		$this.addClass('active on-top');
	}, function() {
		var $this = $(this);

		$this.removeClass('active');

		setTimeout(function(){
			$this.removeClass('on-top');
		},100);
	});

	$('.owl-magaz').owlCarousel({
		nav:false,
		dots:true,
		margin:0,
		items: 1
	});

	/*
	* counts
	*/

	var cardMinus = $(".basket__item-btn-minus");	
	var cardPlus = $(".basket__item-btn-plus");		

	cardMinus.on("click", function(e){
		e.preventDefault();

		var $this = $(this);
		var textContainer = $this.siblings('.basket__item-count-number');
		var text = parseInt( textContainer.html() );
		( text == 0 ) ? "" : textContainer.html(text-1);
	});

	cardPlus.on("click", function(e){
		e.preventDefault();

		var $this = $(this);
		var textContainer = $this.siblings('.basket__item-count-number');
		var text = parseInt( textContainer.html() );
		( text == 99 ) ? "" : textContainer.html(text+1);
	});


	/*
	* map
	*/

	if ( $('#map').length ){
		ymaps.ready(initMap);
	}

	function initMap() {
		var myMap = new ymaps.Map("map", {
			center: [55.747323, 37.396371],
			zoom: 15,
			controls: []
		}),

		myPlacemark2 = new ymaps.Placemark([55.747323, 37.396371], {
			hintContent: 'Собственный значок метки'
		}, {
			iconLayout: 'default#image',
			iconImageHref: '../images/map-pin.png',
			iconImageSize: [79, 86],
			iconImageOffset: [-40, -86]
		});

		myMap.behaviors.disable('scrollZoom');
		myMap.geoObjects.add(myPlacemark2);
	}
}());